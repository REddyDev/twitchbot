# README #

This is a Twitch IRC bot designed to use its own Twitch account to log in and chat in a streamer's channel with other users. It has basic user responses and other more detailed actions such as storing and reading quotes from a database, listing relevant information to the current streamer's stream, and reading and selecting from a list of current users in chat.

### How do I get set up? ###

* Will update with directly relevant steps in the future, for now refer to the [Twitch API Docs](https://dev.twitch.tv/docs/irc/)
