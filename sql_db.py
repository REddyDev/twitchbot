import sqlite3
from sqlite3 import Error

def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        print(sqlite3.version)
        return conn
    except Error as e:
        print(e)
    # finally:
    #     conn.close()
    return None

def create_quote(conn, quote):
    sql = '''INSERT INTO quotes(quote, game, date)
            VALUES (?,?,?)'''

    cur = conn.cursor()
    cur.execute(sql, quote)
    return cur.lastrowid

def select_quote_by_id(conn, id):
    cur = conn.cursor()
    quote = cur.execute("SELECT quote FROM quotes WHERE id=?", (id,)).fetchone()

    print(quote)
    if quote == None:
        return None
    else:
        return quote[0]

def select_random_quote(conn):
    cur = conn.cursor()
    quote = cur.execute("SELECT id, quote FROM quotes ORDER BY RANDOM() LIMIT 1").fetchone()

    print('random quote:')
    print(quote)
    if quote == None:
        return None
    else:
        return quote
