import datetime
import irc.bot
import random
import re
import requests
import sys
# ~~~locals~~~
# import chat_re
import config
import sql_db

username  = config.NICK
alts      = config.ALTS     # shorthand nicknames to call the bot by
token     = config.PASS
client_id = config.C_ID
channel   = config.CHAN
server    = config.HOST
port      = config.PORT
db_conn   = sql_db.create_connection('twitchbot.db')
# random twitch specific urls
gdoc      = config.GDOC
discord   = config.DCRD
pokemon   = config.PKMN

class TwitchBot(irc.bot.SingleServerIRCBot):
    def __init__(self, username, alts, token, channel, server, port, db_conn, client_id, gdoc, discord, pokemon):
        self.username = username
        self.token = token
        self.client_id = client_id
        self.channel = '#' + channel
        self.alts = alts
        self.server = server
        self.port = port
        # sqlite3 db conn local on server
        self.conn = db_conn
        # twitch urls
        self.gdoc = gdoc
        self.discord = discord
        self.pokemon = pokemon

        # create self.chat_re file for pubmsg reading?

        print ('Connecting to ' + server + ' on port ' + str(port) + '...')
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port, token)], username, username)

    def on_welcome(self, c, e):
        print('Joining ' + self.channel)
        # Must request specific capabilities before using them
        c.cap('REQ', ':twitch.tv/membership')
        c.cap('REQ', ':twitch.tv/tags')
        c.cap('REQ', ':twitch.tv/commands')
        c.join(self.channel)

        # chat message to alert successful join
        c.privmsg(self.channel, "Reporting for duty! :)")

    def on_pubmsg(self, c, e):
        # EMOTES TO USE: OWLAna
        msg_lowercase = e.arguments[0].lower()
        # If a chat message starts with a '!' run as a command
        if e.arguments[0][0] == '!':
            cmd = e.arguments[0].split(' ')[0][1:]
            args = e.arguments[0].split(' ')[1:]
            print('Received command: ' + cmd)
            self.do_command(e, cmd, args)

        # just chat with viewers
        # hi bot
        elif re.match('(hi|hello|hey|yo|sup) ' + self.alts, msg_lowercase):
                c.privmsg(self.channel, "HeyGuys")
        # thank bot
        elif re.match('(thank you|thanks|thank|thx) ' + self.alts, msg_lowercase):
            c.privmsg(self.channel, "np bb <3")
        # good bot
        elif re.match('good ' + self.alts, msg_lowercase):
            c.privmsg(self.channel, "TPFufun")
        # bad bot
        elif re.match('bad ' + self.alts, msg_lowercase):
            c.privmsg(self.channel, "I'm sorry :(")
        return

    # def on_follow/sub/whatever

    def do_command(self, e, cmd, args):
        c = self.connection

        # displays current game. really just needed to test API calls
        # allow Mod to change stream game?
        if cmd == 'game':
            headers = {'Client-ID': self.client_id}
            stream_url = 'https://api.twitch.tv/helix/streams/?user_login=' + self.channel
            stream_r = requests.get(stream_url, headers=headers).json()
            if stream_r['data']:
                commenter = ''
                for tag in e.tags:
                    if tag['key'] == 'display-name':
                        commenter = tag['value']
                user_url = 'https://api.twitch.tv/helix/users/?login=' + self.channel
                user_r = requests.get(user_url, headers=headers).json()
                game_url = 'https://api.twitch.tv/helix/games/?id=' + stream_r['data'][0]['game_id']
                game_r = requests.get(game_url, headers=headers).json()
                c.privmsg(self.channel, '@' + commenter + ' - ' + user_r['data'][0]['display_name'] + ' is currently playing ' + game_r['data'][0]['name'] + '!')

        if cmd == 'winner':
            url = 'https://tmi.twitch.tv/group/user/' + self.channel[1:] + '/chatters'
            r = requests.get(url).json()
            viewers = r['chatters']['viewers']
            mods    = r['chatters']['moderators']
            users = viewers + mods
            users.remove('nightbot')
            winner = random.choice(users)
            c.privmsg(self.channel, "And the winner is... @" + winner + " !")

        # Posts a link to Google Doc with all chat commands
        if cmd == 'commands':
            c.privmsg(self.channel, "The list of commands can be found in this Google Doc: " + self.gdoc)

        # Posts a link to the channel's discord
        if cmd == 'discord':
            c.privmsg(self.channel, "Come listen without lag, chat, or just post your dank maymays in discord: " + self.discord)

        # Posts a link to Google Doc for Pokemon names
        if cmd == 'pokemon':
            c.privmsg(self.channel, "List of reserved names for Nuzlocke challenge: " + self.pokemon)

        # Posts shoutout/link to user's twitch channel
        if cmd == 'so':
            if len(args) == 0:
                c.privmsg(self.channel, "Shoutout to everyone here! You guys are awesome TehePelo")
            if len(args) >= 1:
                #  VERIFY THE USER IS ACTUALLY A VIEWER (Twitch API)
                name = args[0]
                if args[0][0] == '@':
                    name = args[0][1:]
                if name.lower() == self.username:
                    c.privmsg(self.channel, "Aww, thx bb :D")
                else:
                    # multiple random variations?
                    c.privmsg(self.channel, "Shoutout to @" + name + " ! Please check them out at twitch.tv/" + name + " CorgiDerp")

        # adds a quote to the database, posts it in chat after
        if cmd == 'addquote':
            string = ' '.join(args)
            # Get current game with Twitch API (do I really even need it?)
            with self.conn:
                # REMOVE "Some Game" REPLACE WITH TWITCH API
                # currently the game is not included in quote, just used in database so technically irrelevant
                quote = (string, 'Some Game', datetime.date.today().strftime('%Y-%m-%d'))
                quote_id = sql_db.create_quote(self.conn, quote)

                c.privmsg(self.channel, "Quote #" + str(quote_id) + ": " + string)

        # Posts selected or random quote in chat
        if cmd == 'quote':
            # by keyword - if int or if str
            # !quote list (link google doc)
            if len(args) == 0:
                with self.conn:
                    quote = sql_db.select_random_quote(self.conn)
                    if quote != None:
                        c.privmsg(self.channel, "Quote #" + str(quote[0]) + ": " + quote[1])
            else:
                with self.conn:
                    quote = sql_db.select_quote_by_id(self.conn, args[0])
                    if quote != None:
                        c.privmsg(self.channel, "Quote #" + args[0] + ": " + quote)

        # delquote? editquote?

        # if cmd == 'hug'
        # if cmd == 'challenge':    #requires points
        # if cmd == 'top5':         #requires points
        # if cmd == 'heist':        #requires points
        # if cmd == 'points':       #StreamLabs API connection?
        # if cmd == 'uptime':
        # if cmd == 'followage':
        # if cmd == '':
        # if cmd == '':

        # if cmd == 'settings':   look up current game, get settings from db
        # if cmd == 'crosshair':   look up current game, get crosshair from db
            # crosshair per character in OW? :\

        # if cmd == 'sr':        song request
        # if cmd == 'currentsong':
        # if cmd == 'murderthestreamer':    ???

def main():
    bot = TwitchBot(username, alts, token, channel, server, port, db_conn, client_id, gdoc, discord, pokemon)
    bot.start()

if __name__ == "__main__":
    main()
